/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: fcs.c
* @Date: Wes, May 23, 2017 at 15:54:12 PM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.1.0
* @bug lists: No known bugs.
*
*/
#include <linux/kernel.h>
#include "fcs.h"

/* Bit flap-flop */
unsigned long long reflect(unsigned long long ref,unsigned char ch)
{     
    int i;  
    unsigned long long value = 0;  
    for( i = 1; i < ( ch + 1 ); i++ )  
    {  
        if( ref & 1 )  
            value |= 1 << ( ch - i );  
        ref >>= 1;
    }
    return value;
}

unsigned int crc32_bit(unsigned char *ptr, unsigned int len, unsigned int gx)
{  
    unsigned char i;  
    unsigned int crc = 0xffffffff;  
    while( len-- )  
    {  
        for( i = 1; i != 0; i <<= 1 )  
        {  
            if( ( crc & 0x80000000 ) != 0 )  
            {  
                crc <<= 1;  
                crc ^= gx;  
            }  
            else   
                crc <<= 1;  
            if( ( *ptr & i ) != 0 )   
                crc ^= gx;  
        }  
        ptr++;  
    }  
    return ( reflect(crc,32) ^ 0xffffffff );  
}  
  
unsigned int Table1[256];  
unsigned int Table2[256];  
  
/* Gererate CRC32 standard table , Second row: 04C11DB7. */
void gen_direct_table(unsigned int *table)
{  
    unsigned int gx = 0x04c11db7;  
    unsigned long i32, j32;  
    unsigned long nData32;  
    unsigned long nAccum32;  
    for ( i32 = 0; i32 < 256; i32++ )  
    {  
        nData32 = ( unsigned long )( i32 << 24 );  
        nAccum32 = 0;  
        for ( j32 = 0; j32 < 8; j32++ )  
        {  
            if ( ( nData32 ^ nAccum32 ) & 0x80000000 )  
                nAccum32 = ( nAccum32 << 1 ) ^ gx;  
            else  
                nAccum32 <<= 1;  
            nData32 <<= 1;  
        }  
        table[i32] = nAccum32;  
    }  
}  

/* Gererate CRC32 ff table , Second row: 77073096. */
void gen_normal_table(unsigned int *table)
{  
    unsigned int gx = 0x04c11db7;  
    unsigned int temp,crc;
    int i;
    int j;
    for(i = 0; i <= 0xFF; i++)   
    {  
        temp=reflect(i, 8);  
        table[i]= temp<< 24;  
        for (j = 0; j < 8; j++)  
        {  
            unsigned long int t1,t2;  
            unsigned long int flag=table[i]&0x80000000;  
            t1=(table[i] << 1);  
            if(flag==0)  
            t2=0;  
            else  
            t2=gx;  
            table[i] =t1^t2 ;  
        }  
        crc=table[i];  
        table[i] = reflect(table[i], 32);  
    }  
}  
  
unsigned int direct_table_crc(unsigned char *ptr,int len, unsigned int * table)
{  
    unsigned int crc = 0xffffffff;   
    unsigned char *p= ptr;  
    int i;  
    for ( i = 0; i < len; i++ )  
        crc = ( crc << 8 ) ^ table[( crc >> 24 ) ^ (unsigned char)reflect((*(p+i)), 8)];  
    return ~(unsigned int)reflect(crc, 32) ;  
}  
  
unsigned int reverse_table_crc(unsigned char *data, int len, unsigned int * table)
{  
    unsigned int crc = 0xffffffff;    
    unsigned char *p = data;  
    int i;  
    for(i=0; i <len; i++)  
        crc =  table[( crc ^( *(p+i)) ) & 0xff] ^ (crc >> 8);  
    return  ~crc ;   
}  


