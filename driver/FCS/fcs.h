/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: fcs.h
* @Date: Wes, May 23, 2017 at 15:55:11 PM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.1.0
* @bug lists: No known bugs.
*
*/
#ifndef FCS_H
#define FCS_H

/* Standard CRC32 poly. */
#define poly  0x04C11DB7
/* Flap-flop CRC32 poly. */
#define upoly 0xEDB88320

unsigned long long reflect(unsigned long long ref,unsigned char ch);
unsigned int crc32_bit(unsigned char *ptr, unsigned int len, unsigned int gx);
void gen_direct_table(unsigned int *table);
void gen_normal_table(unsigned int *table);
unsigned int direct_table_crc(unsigned char *ptr,int len, unsigned int * table);
unsigned int reverse_table_crc(unsigned char *data, int len, unsigned int * table);

#endif

