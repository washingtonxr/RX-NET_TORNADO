/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: virtnet.c
* @Date: Wed, May 11, 2017 at 09:17:35 PM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.1.1
* @bug lists: No known bugs.
*
* The functions flow of the driver is shown bellow:
*
*          [Virtual Network Main functions]
*                _________|________
*               |                  |
*          [rj_wds_rec]       [rj_wds_tx]
*             (Recv)            (Send)
*/
/* Operation System reference headers. */
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/in.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/uaccess.h>
#include <linux/netdevice.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/mm.h>
#include <linux/etherdevice.h>
#include <linux/ip.h>
#include <linux/sched.h>
#include <linux/ioctl.h>
#include <linux/uaccess.h>
#include <linux/wait.h>
#include <linux/skbuff.h>
#include <linux/stddef.h>	/* bool ...*/
#include <linux/slab.h>		/* kmalloc ...*/
#include <linux/atomic.h>
#include <linux/wait.h>
/* Local reference headers. */
#include "virtnet.h"
#ifndef NO_FRAME_FUN
#include "ptp_frame.h"
#endif
#include "ptp_com.h"
#include "ptp_ctl.h"

//static bool EN_AMSDU;
//static bool EN_AMPDU;

#ifndef NO_FRAME_FUN
/* E2W data flow sturcture elements. */
static ptp_mtps_ext_t ext_sig;
#endif

/* Standard module information, edit as appropriate */
MODULE_LICENSE
    ("GPL");
MODULE_AUTHOR
    ("Washington Ruan.");
MODULE_DESCRIPTION
    ("PTP_Adapter - ptp wireless network L2->.");

void get_random_bytes(void *buf, int nbytes);

static struct ptp_device pd[PTP_DEV_NUM];
static struct net_device *ptp_netdev;
//static struct class *recv_class,*send_class;
//static struct device *recv_class_dev,*send_class_dev;
static int timeout = PTP_TIMEOUT;	

/*
 * ptp_net_open.
 */
static int ptp_net_open(struct net_device *dev)
{
#ifdef EN_IRQ
    int ret;
#endif

#ifdef _DEBUG   
    printk("ptp_net_open.\n");
#endif

#ifdef EN_IRQ
    /* Request IRQ etc.*/
    ret = request_irq(dev->irq,&ptp_interrupt,0,dev->name,dev);
#endif
    netif_start_queue(dev);
    return 0;
}

/*
 * ptp_net_close.
 */
static int ptp_net_close(struct net_device *dev) 
{
#ifdef _DEBUG
    printk("ptp_net_close.\n");
#endif

#ifdef EN_IRQ
    free_irq(dev->irq,dev);
#endif    
    netif_stop_queue(dev); 
    return 0;
}

/*
 * ptp_net_get_stats.
 */
static struct net_device_stats *ptp_net_get_stats(struct net_device *dev)
{
    struct ptp_device_priv *local =  dev->ml_priv;
    return &local->stats;
}
/**
 * Virtual network hareware transmit,push data into the 
 * ptp_net_hw_tx device.
 * Date: July,19th,2018.BD->Gift.
 * Author:Washington Ruan Email:washingtonxr@live.com
 */
static int ptp_net_rx(unsigned char *buf, int len, struct net_device *dev)
{
    struct sk_buff *skb;
    struct ptp_device_priv *priv = (struct ptp_device_priv *) dev->ml_priv;

    skb = dev_alloc_skb(len+2);
    if (!skb) {
        printk("ptp_net_rx can't allocate memory to store the packet then drop the packet.\n");
        /* Drop data intentionally.*/
        priv->stats.rx_dropped++;
        return -1;
    }
    skb_reserve(skb, 2);
    /* Push data to skb and ready for sending.*/
    memcpy(skb_put(skb, len), buf, len);

    skb->dev = dev;
    skb->protocol = eth_type_trans(skb, dev);
    /* We need not check the checksum */
    skb->ip_summed = CHECKSUM_UNNECESSARY; 
    /* Update RX part strategy. */
    priv->stats.rx_packets++;
    priv->stats.rx_bytes += len;
    netif_rx(skb);
    return 0;
}

int ptp_net_tornado(struct chardev_ext_inf *info)
{
	unsigned char tx_buf[1518];
	// Eth II
	unsigned char SA[6] = {0x00,0x00,0x5e,0x10,0x20,0x30};
	unsigned char DA[6] = {0x00,0x00,0x5e,0x10,0x20,0x31};
	// TYPE
	unsigned char E2TYPE[2] = {0x08,0x00};
	// IPV4 -> ICMP
	// TYPE
	unsigned char IPTYPE[1] = {0x45};
	// DF
	unsigned char DFSF[1] = {0};
	// T LEN
	unsigned char TLEN[2] = {0,};
	// Identification
	unsigned char IDTI[2] = {0x12,0x2a};
	// Flags
	unsigned char FLAG[1] = {0};
	// Fragment offset
	unsigned char FROF[2] = {0,};
	// Time to live
	unsigned char TTLV[1] = {0x40};
	// Protocol
	unsigned char PROT[1] = {0x01};
	// Header Csum
	unsigned char HCSM[2] = {0,};
	// Source IP
	unsigned char SIP[4] = {0,};
	// Destination IP
	unsigned char DIP[4] = {0,};
	
	// FCS
	unsigned int FCS = 0x11223344;
	
	unsigned int buf_len = 0, prt = 0;
	unsigned int i = 0;
	unsigned long rnum = 0;
	unsigned int rt = 0;
	
	if(info->Try_time == 0){
		printk("Notice: info->Try_time = 0.\n");
		return 0;
	}else if(info->Buf_lens > 1518){
		printk("Error:info->Buf_lens > 1518.\n");
		return 0;	
	}
	for(rt = 0; rt <info->Try_time; rt++){
		prt = 0;
		memset(tx_buf,0,sizeof(tx_buf));
		memcpy(tx_buf+prt,SA,sizeof(SA));
		prt += sizeof(SA);
		memcpy(tx_buf+prt,DA,sizeof(DA));
		prt += sizeof(DA);
		memcpy(tx_buf+prt,E2TYPE,sizeof(E2TYPE));
		prt += sizeof(E2TYPE);
		memcpy(tx_buf+prt,IPTYPE,sizeof(IPTYPE));
		prt += sizeof(IPTYPE);	
		memcpy(tx_buf+prt,DFSF,sizeof(DFSF));
		prt += sizeof(DFSF);	
		memcpy(tx_buf+prt,TLEN,sizeof(TLEN));
		prt += sizeof(TLEN);	
		memcpy(tx_buf+prt,IDTI,sizeof(IDTI));
		prt += sizeof(IDTI);	
		memcpy(tx_buf+prt,FLAG,sizeof(FLAG));
		prt += sizeof(FLAG);
		memcpy(tx_buf+prt,FROF,sizeof(FROF));
		prt += sizeof(FROF);	
		memcpy(tx_buf+prt,TTLV,sizeof(TTLV));
		prt += sizeof(TTLV);
		memcpy(tx_buf+prt,PROT,sizeof(PROT));
		prt += sizeof(PROT);	
		memcpy(tx_buf+prt,HCSM,sizeof(HCSM));
		prt += sizeof(HCSM);
		memcpy(tx_buf+prt,SIP,sizeof(SIP));
		prt += sizeof(SIP);
		memcpy(tx_buf+prt,DIP,sizeof(DIP));
		prt += sizeof(DIP);
		printk("prt=%d.\n",prt);
		if(info->Buf_lens >= 0){
			if(info->Buf_lens == 0){
				get_random_bytes(&rnum, sizeof(unsigned long));
				buf_len = 64 + rnum%(1518-64-prt);					
			}else{
				buf_len = info->Buf_lens;
			}
			
			printk("1,buf_len=%d\n",buf_len);	
			
			for(i = 0; i < buf_len; i++){
				get_random_bytes(&rnum, sizeof(unsigned long));
				tx_buf[i+prt] = rnum%256;
			}
			prt += buf_len;
		}

		//memcpy(tx_buf+prt,FCS,sizeof(FCS));
		//prt+=sizeof(FCS);
		buf_len = prt;
		printk("2,buf_len=%d\n",buf_len);
		if(buf_len <= 1518)
			ptp_net_rx(tx_buf, buf_len, ptp_netdev);
	}
	return 0;
}

/**
 * Virtual network hareware transmit,push data into the 
 * ptp_net_hw_tx device.
 */
static void ptp_net_hw_tx(char *buf, int len, struct net_device *dev)
{
    struct ptp_device_priv *priv = NULL;
	int res;
	const unsigned char default_da[6]={0x00,0x00,0x5e,0x01,0x02,0x03};    /* Temporary set right here.*/
    
    /* check the ip packet length,it must more then 34 octets */
    if (len < sizeof(struct ethhdr) + sizeof(struct iphdr)) {
        printk("Bad packet! It's size is less then 34!\n");
        return;
    }
    if(memcmp(buf,default_da,6)==0){
        printk("Got a valid package(%dBytes).\n",len);
    }
#if 0    
    /* now push the EthernetII RAM data to ... */ 
	//pd[PTP_SEND_DEVICE].ptp_kernel_write(buf,len,pd[PTP_SEND_DEVICE].buffer_size);
	if(memcmp(buf,default_da,6)==0){
		//down(&ext_sig.msgl_lock);
		switch(atomic_read(&ext_sig.msbuff_prt)){
		case 0:	/* Buffer A. */
			if(atomic_read(&ext_sig.msbuff_afull)==0){
				res = check_amsdu_len_threshold(ext_sig.msbuff_a_len, len, ext_sig.mcs_lev);	/* Packup prestore package. */
				if(res > 0){
					atomic_inc(&ext_sig.msbuff_afull);
					/* Check if buffer b is available or not? */
					if(atomic_read(&ext_sig.msbuff_bfull)==0){
						res = check_amsdu_len_threshold(ext_sig.msbuff_b_len, len, ext_sig.mcs_lev);	/* Packup prestore package. */
						if(res <=0){
							res = append_msdu(&ext_sig.msbuff_b, buf, len);  
							if(res < 0)
								printk("Append ethernet buff error.\n");
							/* Modify a_msdu_len and wait for checking if it is full or not. */
							ext_sig.msbuff_b_len += len + sizeof(ptp_amsdu_header_t);
							atomic_inc(&ext_sig.msbuff_prt);
						}else{ 
							//atomic_set(&ext_sig.msbuff_prt,2);
							break;
						}
					}else 
						break;
				}else{
					res = append_msdu(&ext_sig.msbuff_a, buf, len);  
					if(res < 0)
						printk("Append ethernet buff error.\n");
					/* Modify a_msdu_len and wait for checking if it is full or not. */
					ext_sig.msbuff_a_len += len + sizeof(ptp_amsdu_header_t);
				}
			}
			/* Else drop data. */
		break;
		case 1:	/* Buffer B. */
		if(atomic_read(&ext_sig.msbuff_bfull)==0){
			res = check_amsdu_len_threshold(ext_sig.msbuff_b_len, len, ext_sig.mcs_lev);	/* Packup prestore package. */
			if(res > 0){
				atomic_inc(&ext_sig.msbuff_bfull);	
				/* Check if buffer a is available or not? */
				if(atomic_read(&ext_sig.msbuff_afull)==0){
					res = check_amsdu_len_threshold(ext_sig.msbuff_a_len, len, ext_sig.mcs_lev);	/* Packup prestore package. */
					if(res <= 0){
						res = append_msdu(&ext_sig.msbuff_a, buf, len);  
						if(res < 0)
							printk("Append ethernet buff error.\n");
						/* Modify a_msdu_len and wait for checking if it is full or not. */
						ext_sig.msbuff_a_len += len + sizeof(ptp_amsdu_header_t);
						atomic_dec(&ext_sig.msbuff_prt);
					}else{
						//atomic_set(&ext_sig.msbuff_prt,2);
						break;
					}
				}else 
					break;
			}else{
				res = append_msdu(&ext_sig.msbuff_b, buf, len);  
				if(res < 0)
					printk("Append ethernet buff error.\n");
				/* Modify a_msdu_len and wait for checking if it is full or not. */
				ext_sig.msbuff_b_len += len + sizeof(ptp_amsdu_header_t);
			}
		}
		/* Else drop data. */
		break;
		default:
			printk("EthernetII data is droped.\n");
		break;
		}
		//up(&ext_sig.msgl_lock);
	}
#endif
	/* Record the transmitted packet status */
	priv = (struct ptp_device_priv *) dev->ml_priv;
	priv->stats.tx_packets++;
	priv->stats.tx_bytes += len;
    
    /* Remember to free the sk_buffer allocated in upper layer. */
    dev_kfree_skb(priv->skb);
	
	return;
}

/**
 * Dealing with a transmit timeout issue.
 */
static void ptp_net_tx_timeout(struct net_device *dev)
{
    struct ptp_device_priv *priv = (struct ptp_device_priv *) dev->ml_priv;
    
    printk("Transmit timed out.\n");
    
    netif_trans_update(dev); 		/* prevent tx timeout */
    
    priv->stats.tx_errors++;
    priv->stats.tx_dropped++;
    
    netif_wake_queue(dev);
    return;
}

/**
 * Transmit the packet,called by the kernel when there is an
 * application wants to transmit a packet.
 */
static int ptp_net_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
    int len;
    char *data,minpkt[ETH_ZLEN];
    struct ptp_device_priv *priv = (struct ptp_device_priv *) dev->ml_priv;
    
#if 0
    if( pd[PTP_SEND_DEVICE].busy == 1){
        printk("pd[PTP_SEND_DEVICE].busy == 1\n");
        return -1;
    }
#endif

    len = skb->len;

    if(len < ETH_ZLEN){
        memset(minpkt,0,sizeof(minpkt));
        memcpy(minpkt,skb->data,skb->len);
        len = ETH_ZLEN;
        data = minpkt;
    }else{
        data = skb->data;
    }
    
    //netif_trans_update(dev); /* prevent tx timeout */
    
    /* remember the skb and free it in ptp_net_hw_tx */
    priv->skb = skb;
    dev_trans_start(dev);
    
    /* pseudo transmit the packet,hehe */
    ptp_net_hw_tx(data, len, dev);
    return 0; 
}

/**
 * Alloclate a space of mamory to set mtu.
 */
static int ptp_net_realloc(int new_mtu)
{
    int err;
    int i;
    char *local_buffer[PTP_DEV_NUM];
    int size;
	
	err = -1;
    
    for (i=0;i<PTP_DEV_NUM;i++){
        
        local_buffer[i] = kmalloc(new_mtu + 4,GFP_KERNEL);
        
        size = min(new_mtu,pd[i].buffer_size);
    
        memcpy(local_buffer[i],pd[i].buffer,size);
        
        kfree(pd[i].buffer);
        
        pd[i].buffer = kmalloc(new_mtu + 4,GFP_KERNEL);
        
        if( pd[i].buffer < 0){
            printk("Can't realloc memory from kernel when refleash a new MTU.\n");
            return err;
        }
    }
    return 0;
}

/**
 * TCP/IP may call the function, if its necessary.
 */
static int ptp_net_change_mtu(struct net_device *dev, int new_mtu)
{
    int err;
    unsigned long flags;
    
    spinlock_t *lock = &((struct ptp_device_priv *) dev->ml_priv)->lock;
    
    /* en, the mtu CANNOT LESS THEN PTP_MUT_MIN_Threshold OR MORE THEN PTP_MUT_MAX_Threshold. */
    if (new_mtu < PTP_MUT_MIN_Threshold)
        return -1;
    
    spin_lock_irqsave(lock, flags);
    dev->mtu = new_mtu;
    
    /* realloc the new buffer */
    err = ptp_net_realloc(new_mtu);
    spin_unlock_irqrestore(lock, flags);
    return err; 
}

/**
 * Set a new MAC address for the device. Use ptp address in the future.
 */
static int ptp_net_set_mac_address(struct net_device *dev, void *p)
{
#if 0    
    struct sockaddr *address = addr;
    if(!is_valid_ether_addr(address->sa_data))
        return -EADDRNOTAVAIL;
    memcpy(dev->dev_addr,address->sa_data,dev->addr_len);
#endif
    return 0;
}

/**
 * Append function when need.
 */
static int ptp_net_ioctl(struct net_device *dev, struct ifreq *rq, int cmd)
{
    return 0;
}

/**
 * Ptp_net_config.
 */
int ptp_net_config(struct net_device *dev, struct ifmap *map)
{
    if (dev->flags & IFF_UP) 
        return -1;
    
    /* Try to modify the io_base addr. */
    if (map->base_addr != dev->base_addr) {
        printk(KERN_WARNING "PTP_IF: Can't modify IO address.\n");
        return -1;
    }
    
    /* Capable to change the irq. */
    if (map->irq != dev->irq) {
        dev->irq = map->irq;
    }
    return 0;
}

/**
 * Creat header.
 */
static int ptp_header_create (struct sk_buff *skb, struct net_device *dev,
			   unsigned short type, const void *daddr,
			   const void *saddr, unsigned int len)
{
    struct ethhdr *eth = (struct ethhdr *)skb_push(skb,ETH_HLEN);
    eth->h_proto = htons(type);
    memcpy(eth->h_source, saddr? saddr : dev->dev_addr,dev->addr_len);
    memcpy(eth->h_dest, daddr? daddr : dev->dev_addr, dev->addr_len);
    return (dev->hard_header_len);
}
               
/*
 * Initialize header_ops for network device.
 */
static const struct header_ops ptp_header_ops = {
	.create	= ptp_header_create,
};
    
/*
 * Initialize net_device_ops for network device.
 */
static const struct net_device_ops ptp_netdev_ops = {
	.ndo_open               =   ptp_net_open,
	.ndo_stop               =   ptp_net_close,
    .ndo_get_stats          =   ptp_net_get_stats,
    .ndo_start_xmit         =   ptp_net_start_xmit,
    .ndo_change_mtu         =   ptp_net_change_mtu,
    .ndo_set_mac_address    =   ptp_net_set_mac_address,
    .ndo_do_ioctl           =   ptp_net_ioctl,              /* Just set temporary*/
    .ndo_set_config         =   ptp_net_config,
    .ndo_tx_timeout         =   ptp_net_tx_timeout,
};

/**
 * Initialize virtual network device.
 */
static void ptp_netdev_setup(struct net_device *dev)
{
    //struct ptp_device_priv *priv;
#ifdef INIT_HW
    /* Initialize the hardware.*/
    xxx_hw_init();
#endif
    /* Set mac address templorary.*/
    memcpy(dev->dev_addr, "\0ED000", ETH_ALEN);
    ether_setup(dev); 
    
    dev->netdev_ops = &ptp_netdev_ops;
    dev->header_ops = &ptp_header_ops;
    
    //dev->ethtool_ops = &ptp_ethtool_ops;
    dev->watchdog_timeo = timeout;
    dev->flags |= IFF_NOARP;
    
    /* Get network device private data. */
    dev->ml_priv = kmalloc(sizeof(struct ptp_device_priv), GFP_KERNEL);
    if (dev->ml_priv == NULL){
        printk("Kernel memory allocate for det->priv failed.\n");
        return;
    }
    
    //priv = netdev_priv(ptp_netdev);    
    memset(dev->ml_priv, 0, sizeof(struct ptp_device_priv));
    spin_lock_init(& ((struct ptp_device_priv *)dev->ml_priv)->lock);
    return;
}
/**
 * Initialize virtual network device.
 */
int ptp_netdev_init(void)
{
    int err;

    ptp_netdev = alloc_netdev(0, PTP_IF, NET_NAME_UNKNOWN, ptp_netdev_setup);
    if (!ptp_netdev)
		return -1;
    
	err = register_netdev(ptp_netdev);
	if (err < 0) {
        printk("%s: error %i registering virtual network device \"%s\"\n",PTP_IF, err, ptp_netdev->name);
		free_netdev(ptp_netdev);
		return err;
	}
	msleep(5000);

#ifndef NO_MTDS_FUN
    /* Initialize linklists for MSDU and MPDU. */
	init_ampdu(&ext_sig.mpbuff_ext);	/* Signal MPDU in first stage. */
	memset(&ext_sig,0,sizeof(ptp_mtps_ext_t));
	init_amsdu(&ext_sig.msbuff_a);      /* Aggregate MSDU enable. */
	init_amsdu(&ext_sig.msbuff_b);	    /* Aggregate MSDU enable. */
    ext_sig.mcs_lev = 0;
    /* Start _mppack_task & _push_task thread. */
	err = init_bg_tasks(&ext_sig);
	if (err < 0) {
		printk("Initialize background tasks failed.\n");
	}
#endif
	return err;    
}
	
/**
 * Called by the kernel to setup both character devices(wds_tx/wds_rx) 
 * and network device module.
 */
static int virtnet_init(void)
{
    int err;
    err = ptp_netdev_init();
    if(err < 0){
        printk("Initilaizing ptp_net_dev_init failed.\n");
        return err;
    }
    err = ptp_ctl_init(&ext_sig);
    if(err < 0){
        printk("Initilaizing ptp_ctl_init failed.\n");
        return err;
    }   
    return 0;	
}
static int __init ptp_adpter_init(void)
{
    int err;
    /* Initialize virtnet adapter. */
    err = virtnet_init();
    /* Initialize AXIS-DMA channel. */
    
    /* Initialize AXI-Lite channel. */
    return err;
}

/**
 * Clean up the network device1.
 */
static void ptp_netdev_cleanup(void)
{
    int err;
	/* Retrieve resources for net device. */
	unregister_netdev(ptp_netdev);
    free_netdev(ptp_netdev);
    kfree(ptp_netdev->ml_priv);

#ifndef NO_MTDS_FUN
	/* Stop tasks in background. */
	err = exit_bg_tasks();
	if (err < 0) {
		printk("Exit background tasks failed.\n");
	}
#endif
#ifndef NO_FRAME_FUN
    
    /* Clear all data in this link list. */
    clear_amsdu(ext_sig.msbuff_a);
    clear_amsdu(ext_sig.msbuff_b);
    clear_ampdu(ext_sig.mpbuff_ext);
#endif
    return;
}

/**
 * Virtnet_exit and retrieve all assigned resources.
 */
static void virtnet_exit(void)
{
    /* Retrieve resoureces were assigned to network devices.*/
    ptp_netdev_cleanup();
    
    /* Ctl exit*/
    ptp_ctl_exit();
    
    return;
}

/**
 * ptp_adpter_exit and retrieve all assigned resources.
 */
static void __exit ptp_adpter_exit(void)
{
    /* Exit virtnet adapter. */
    virtnet_exit();
    /* Exit AXIS-DMA channel. */
    
    /* Exit AXI-Lite channel. */
}

module_init(ptp_adpter_init);
module_exit(ptp_adpter_exit);

