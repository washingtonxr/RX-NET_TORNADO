/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: virtnet.h
* @Date: Wed, May 11, 2017 at 12:14:08 PM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.1.1
* @bug lists: No known bugs.
*
*/

#ifndef _VIRTNET_H_
#define _VIRTNET_H_

#include <linux/netdevice.h>

#define PTP_RECV_DEVICE			0
#define PTP_SEND_DEVICE			1
#define PTP_DEV_NUM				2
/* Define for charactor devices. */
#define PTP_RECV_DEV_NAME		"ptp_rx"
#define PTP_SEND_DEV_NAME		"ptp_tx"

#define PTP_MTU             	192
#define PTP_MUT_MIN_Threshold	68
#define PTP_MUT_MAX_Threshold	1500
#define PTP_MAGIC				0x999
#define PTP_BUFFER_SIZE			2048
#define PTP_TIMEOUT				5
#define PTP_IF					"netT"
#define PTP_IF_NAME_LEN			4

//#define _DEBUG
#undef NULL
#define NULL ((void *)0)

//#define NO_FRAME_FUN	/* No frame functions. */
//#define NO_MTDS_FUN		/* No multi threads. */

/* this is the private data struct of ptp_device */
struct ptp_device{
	int magic;
	char name[PTP_IF_NAME_LEN]; 	
	int busy;
	unsigned char *buffer;
    wait_queue_head_t rwait;
	int mtu;
	spinlock_t lock;
	int tx_len;
    int rx_len;
    int buffer_size;
	struct file *file;
    ssize_t (*ptp_kernel_write)(const char *buffer,size_t length,int buffer_size);
    //ssize_t (*kernel_write)(struct file *file, const char *buf, size_t count, loff_t pos);
};
/* this is the private data struct of ptp_device */
struct ptp_device_priv {
    struct net_device_stats stats;
    struct sk_buff *skb;
    spinlock_t lock;
};


#endif


