/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: ptp_frame.h
* @Date: Wed, May 16, 2017 at 19:18:40 PM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.1.17
* @bug lists: No known bugs.
*
*/
#ifndef PTP_FRAME_H
#define PTP_FRAME_H
#include <linux/wait.h>
//#include "ptp_com.h"

#define AM_INDI             0x55
#define MCS_BASE_RB         (318*64)
#define RB_DATA_WIDTH(x)	(MCS_BASE_RB*(x + 1))
#define FCS_LEN		4   /* FCS length. */

/* Service data frame ===> */

/* Structure for MSDU.*/
typedef struct ptp_msdu ptp_msdu_t;
struct ptp_msdu{
	/* Address assignement.*/
	__u8 s_add[6],				/* Source hardware address.*/
		 d_add[6];				/* Destination hardware .*/
	struct{
		__u16 len;				/* Body lenght. */
		char *buff;				/* Frame body. */
	};
};

/* Structure for AMSDU header.*/
typedef struct ptp_amsdu_header ptp_amsdu_header_t;
struct ptp_amsdu_header{
	/* Address assignement.*/
	__u8 s_add[6],				/* Source hardware address.*/
		 d_add[6];				/* Destination hardware .*/
	__u32 resv;					/* Reserved. */
};

/* Structure for AMSDU frame body.*/
typedef struct ptp_amsdu_fb ptp_amsdu_fb_t;
struct ptp_amsdu_fb{
	/* Frame guard space. */
	struct {
		__u8 indi;				/* Guard Indicator */
		__u8 seq;				/* Guard Sequence */
		__u16 len;				/* Body data lenght */
	};
	/* Frame body. */
	char *buff;					/* Data buffer */
};

/* Structure for AMSDU.*/
typedef struct ptp_amsdu ptp_amsdu_t;
struct ptp_amsdu{
	struct {
		ptp_amsdu_header_t header;	/* Header */
		ptp_amsdu_fb_t body;		/* Frame amsdu body */
		//__u32 FCS;				/* FCS */	
	};
	ptp_amsdu_t *next;				/* Pointer to next. */
	ptp_amsdu_t *prev;				/* Pointer to preview. */
};

/* Protocol frmae ===> */

/* Structure for wireless mac header. */
typedef struct ptp_mHeader ptp_mHeader_t;
struct ptp_mHeader{
	/* MAC Header. */
	struct {
		/* Frame Control. */
		__u16	ver:2,			/* Version */
				m_type:2,		/* Main type */
				s_type:4,		/* Subtype */
				extrn:1,		/* Extern */
				aggre:1,		/* Aggregate for MSDU */
				retry:2,		/* Retry times */
				more_d:1,		/* More data */
				ack:1,			/* ACK enable or not */
				fc_resv:2;		/* Frame control reserved bits */
		/* Address assignement.*/
		__u8	s_add[6],		/* Source hardware address.*/
				d_add[6];		/* Destination hardware address.*/
		/* Body data lenght. */
		__u16	body_len;		/* Body data length */
		__u16	seq;			/* Frame sequence */
		__u16	qos;			/* QoS service */
		//__u32	f_resv;			/* Frame reserved */
	};
};

/* Structure for single MPDU*/
typedef struct ptp_signle_mpdu ptp_signle_mpdu_t;
struct ptp_signle_mpdu{
	struct{
		ptp_mHeader_t header;	/* MAC Header. */
		ptp_amsdu_t *body;		/* Frame body. */
		__u32 FCS;				/* FCS. */
	};
	//wait_queue_head_t rwait;	/* Wait interrupt. */
	//__u32 en_cpy_sig;			/* Wakeup packer signal. */
	//spinlock_t lock;			/* Siplock sign. */
	ptp_signle_mpdu_t *next;	/* Pointer to next. */
	ptp_signle_mpdu_t *prev;	/* Pointer to preview. */
};

/* Function frame body ===> */

/* Structure for ACK. */
typedef struct ptp_ack_fb ptp_ack_fb_t;
struct ptp_ack_fb{
	/* ACK mode. */
	__u32 mode:2,           	/* ACK mode */
		data_bs:8,	        	/* Data block sequence */
		am_resv:22;				/* ACK mode reserved bits */
	/* ACK sign for each frame.*/
	__u32 a_sign;	        	/* Each bit relate to a MSUD block. */
};

/* Structure for Associating. */
typedef struct ptp_asso_fb ptp_asso_fb_t;
struct ptp_asso_fb{
	/* Associate mode. */
	__u8 mode:2,
		am_resv:6;
	/* Associate control data. */
	struct{
		__u16 qos;				/* QoS */
		__u16 mcs;				/* MCS */
		__u16 status;			/* Status */
		__u8 encry_data[64];	/* Encrypt Data */
		__u8 resv[2];	    	/* Reserved bytes */
	};
};

/* Structure for probing. */
typedef struct ptp_probe_fb ptp_probe_fb_t;
struct ptp_probe_fb{
	/* Probe mode. */
	__u8 mode:2,            	/* Porbe mode */
		pm_resv:6;          	/* Probe frame reserved */
	/* Only exist when reply probing. */
	struct {
		__u16 local_rssi;		/* Local RSSI value */
		__u16 pair_rssi;		/* Pair point RSSI value */
		__u16 noise_floor;		/* Noise floor value */
		__u16 cqi;				/* CQI value */
		__u8 compress_h[8];		/* Compress h matri value */
		__u8 resv[2];   		/* Reserved bytes */
	};
};

/* External function list. */
void init_amsdu(ptp_amsdu_t **elist);
void init_ampdu(ptp_signle_mpdu_t **elist);
void clear_amsdu(ptp_amsdu_t *elist);
void clear_ampdu(ptp_signle_mpdu_t *elist);
int append_msdu(ptp_amsdu_t **eNode, char *buff, int len);
int check_amsdu_len_threshold(unsigned int aggr_len, unsigned int buf_len, unsigned char mcs_level);
void print_amsdu_db(ptp_amsdu_t *msdu_head);
unsigned int check_amsdu_num(ptp_amsdu_t *msdu_head);
//int append_mpdu(ptp_amsdu_t *msdu_head, ptp_mHeader_t *mpdu_Head, ptp_signle_mpdu_t **mpdu_eNode, unsigned int aggr_len, unsigned char mcs_level);
int append_mpdu(ptp_amsdu_t *msdu_head, ptp_mHeader_t *mpdu_Head, unsigned int aggr_len, unsigned char mcs_level);

void print_ampdu_db(ptp_signle_mpdu_t *mpdu_head);
int show_mpdu(ptp_signle_mpdu_t *mpdu_head);
int check_amsdu_len_threshold(unsigned int aggr_len, unsigned int buf_len, unsigned char mcs_level);
int duplicate_mpdu_db(ptp_signle_mpdu_t *mpdu_head, ptp_signle_mpdu_t *bk_mpdu_head);

#endif

