/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: ptp_com.c
* @Date: Wed, May 22, 2017 at 16:19:12 PM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.2.0
* @bug lists: No known bugs.
*
*/
#ifndef PTP_CTL_H
#define PTP_CTL_H

#include <linux/device.h>           // Definitions for class and device structs
#include <linux/cdev.h>             // Definitions for character device structs
#include "ptp_com.h"

#define DRIVER_NAME     "nTornado"
#define DRIVER_MAJ      0//202
#define DRIVER_MIN      0
#define MODULE_NAME     "nTdriver"

// Truncates the full __FILE__ path, only displaying the basename
#define __FILENAME__ \
    (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

// Convenient macros for printing out messages to the kernel log buffer
#define chardev_err(fmt, ...) \
    printk(KERN_ERR MODULE_NAME ": %s: %s: %d: " fmt, __FILENAME__, __func__, \
           __LINE__, ## __VA_ARGS__)
#define chardev_info(fmt, ...) \
    printk(KERN_INFO MODULE_NAME ": %s: %s: %d: " fmt, __FILENAME__, __func__, \
            __LINE__, ## __VA_ARGS__)

struct chardev_ctl_dev {
    int num_devices;                // The number of devices
    unsigned int minor_num;         // The minor number of the device
    dev_t dev_num;                  // The device number of the device
    char *chrdev_name;              // The name of the character device
    struct device *device;          // Device structure for the char device
    struct class *dev_class;        // The device class for the chardevice
    struct cdev chrdev;             // The character device structure
};

struct chardev_ext_inf{
    unsigned char SA[6];            // Source Address.
    unsigned char DA[6];            // Destination Address.
    unsigned int Try_time;          // Try_times.
    unsigned int Buf_lens;          // Buffer data length.
    unsigned char Buf_type;         // Buffer type.
    unsigned char Mode;             // Mode.
};
#define PTP_CTL_MAGIC              'R'
#define AXIDMA_CTL_NUM_IOCTLS		1
#define PTP_CTL_IO	_IOWR(PTP_CTL_MAGIC, 0, struct chardev_ext_inf)
/*
struct chardev_ext_inf{
    int magic;
    char name[8]; 	
    int busy;
    unsigned char *buffer;
    wait_queue_head_t rwait;
    int mtu;
    spinlock_t lock;
    int td_len;
    int rd_len;
    int buf_size;
    int cmd;
    struct file *file;
    ssize_t (*kernel_write)(const char *buffer,size_t length,int buffer_size);
};
*/
int ptp_ctl_init(ptp_mtps_ext_t *ext_sig);
void ptp_ctl_exit(void);
extern int ptp_net_tornado(struct chardev_ext_inf *info);



#endif

