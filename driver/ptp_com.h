/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: ptp_com.h
* @Date: Tur, May 17, 2017 at 9:21:45 AM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.1.17
* @bug lists: No known bugs.
*
*/
#ifndef PTP_COM_H
#define PTP_COM_H

#include <linux/types.h>
#include <linux/wait.h>
#include <linux/kernel.h>
#include <linux/semaphore.h>
#include <linux/atomic.h>

#include "ptp_frame.h"

#define MPDU_TO		5   /* MPDU 1ms timeout. */

/*!< \brief COMMON layer error reporting enumerated types */
typedef enum
{
	COMINFO_OK=0,
	COMINFO_ERR,
} cominfo_t;

/*
 * Kernel specific type declarations for XFS
 */
typedef signed char		__int8_t;
typedef unsigned char		__uint8_t;
typedef signed short int	__int16_t;
typedef unsigned short int	__uint16_t;
typedef signed int		__int32_t;
typedef unsigned int		__uint32_t;
typedef signed long long int	__int64_t;
typedef unsigned long long int	__uint64_t;

typedef __uint32_t __u32;
typedef __int32_t __s32;
typedef __uint16_t __u16;
typedef __int16_t __s16;
typedef __uint8_t __u8;

/* Structure for mtps_ext_t.*/
typedef struct ptp_mtps_ext ptp_mtps_ext_t;
struct ptp_mtps_ext{
    unsigned char mcs_lev;
	/* Multipule thread common stuffs. */
    wait_queue_head_t _wait_queue;	/* Wait queue. */
    //spinlock_t lock;
	struct semaphore msgl_lock;		/* Global lock. */
	//struct mutex db_lock;
	//unsigned int ms_cpy_sig;
	
	/* A-MPDU stuffs. */
	unsigned int a_mpdu_len;
    ptp_signle_mpdu_t *mpbuff_ext;

	/* A-MSDU stuffs. */
	atomic_t msbuff_prt;	/* Pointer. */
	atomic_t msbuff_afull, msbuff_bfull;	/* Buffer write enable sign. */
	unsigned int msbuff_a_len,msbuff_b_len;	/* Buffer length. */
    ptp_amsdu_t *msbuff_a,*msbuff_b;		/* Buffer. */
};

int init_bg_tasks(ptp_mtps_ext_t *ext_sig);
int exit_bg_tasks(void);

#endif

