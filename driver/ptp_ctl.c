/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: ptp_com.c
* @Date: Wed, May 22, 2017 at 16:19:12 PM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.2.0
* @bug lists: No known bugs.
*
*/
#include <linux/fs.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/list.h>             // Linked list definitions and functions
#include <linux/kernel.h>           // Contains the definition for printk
#include <linux/device.h>           // Definitions for class and device structs
#include <linux/cdev.h>             // Definitions for character device structs
#include <linux/signal.h>           // Definition of signal numbers
#include <linux/uaccess.h>
//#include <asm/ioctl.h>              // IOCTL macros
//#include <asm/uaccess.h>
//#include <asm/irq.h>
//#include <asm/io.h>

#include "ptp_ctl.h"
#include "virtnet.h"

static struct chardev_ctl_dev dev;
static ptp_mtps_ext_t *local_sig;

static int ptp_ctl_open(struct inode *inode,struct file *file)
{
    // Place the chardev_ctl_dev structure in the private data of the file
    //file->private_data = (void *)chardev_0;
    return 0;
}
static int ptp_ctl_release(struct inode *inode, struct file *file)
{
    //file->private_data = NULL;
    return 0;
}
static long ptp_ctl_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    void *__user arg_ptr;
    volatile unsigned long *local_reg = NULL;
    static struct chardev_ext_inf info;
    /* Coerce the arguement as a userspace pointer.*/
    arg_ptr = (void __user *)arg;
    switch (cmd) {
		case PTP_CTL_IO:
            if (copy_from_user(&info, arg_ptr, sizeof(info)) != 0) {
                chardev_err("Unable to copy info from userspace.\n");
                return -EFAULT;
            }
			if(info.Mode==0){
				printk("Send data to bridge.\n");
				ptp_net_tornado(&info);
				printk("Try time=%d\n",info.Try_time);
			}

		break;
        default:
            chardev_err("Invalid ioc command.\n");
            //chardev_info("Get ioctl:val.addr=%x,val.offset=%x,val.data=%x.\n",val.addr,val.offset,val.data);
            return -EFAULT;
        break;
    }
#if 0
    /* Perform the specified command.*/
    switch (cmd) {
        case L1CHIP_CTL_REG:
            if (copy_from_user(&val, arg_ptr, sizeof(val)) != 0) {
                chardev_err("Unable to copy val from userspace.\n");
                return -EFAULT;
            }
            if(val.offset > 0xffff ){
                chardev_err("val.offset = %d which is out of range.\n",val.offset);
                return -EFAULT;
            }
            //chardev_info("Get ioctl from L1CHIP_CTL_REG:val.addr=%x,val.offset=%x,val.data=%x.\n",val.addr,val.offset,val.data);
            if(val.addr == APB_M1_ADDRESS){
                // Write data to address.
                local_reg = ioremap(val.addr + val.offset, 4);
                if (!local_reg) {
                    chardev_err("ENOMEM:val.addr=%x,val.offset=%x,val.data=%x.\n",val.addr,val.offset,val.data);
                    return -ENOMEM;
                }
                *local_reg = val.data;

                val.data = *local_reg;
                iounmap(local_reg);
            }else if(val.addr == APB_TB_ADDRESS){
                /* Read data from registers by singel. */
                val.addr = 0x01010101;
                val.offset ++;
                val.data ++;                
            }else{
                chardev_err("Invalid ioc address.\n");
                chardev_info("Get ioctl:val.addr=%x,val.offset=%x,val.data=%x.\n",val.addr,val.offset,val.data);
                return -EFAULT;
            }

            /* Write data back to userspace.*/
            if (copy_to_user(arg_ptr, &val, sizeof(val)) != 0) {
                chardev_err("Unable to copy val to userspace.\n");
                return -EFAULT;
            }
        break;
        case L1CHIP_CTL_WREG:
            if (copy_from_user(&val, arg_ptr, sizeof(val)) != 0) {
                chardev_err("Unable to copy val from userspace.\n");
                return -EFAULT;
            }
            if(val.offset > 0xffff ){
                chardev_err("val.offset = %d which is out of range.\n",val.offset);
                return -EFAULT;
            }
            //chardev_info("Get ioctl from L1CHIP_CTL_REG:val.addr=%x,val.offset=%x,val.data=%x.\n",val.addr,val.offset,val.data);
            if(val.addr == APB_M1_ADDRESS){
                // Write data to address.
                local_reg = ioremap(val.addr + val.offset, 4);
                if (!local_reg) {
                    chardev_err("ENOMEM:val.addr=%x,val.offset=%x,val.data=%x.\n",val.addr,val.offset,val.data);
                    return -ENOMEM;
                }
                *local_reg = val.data;
                // Copy data from actually address.
                iounmap(local_reg);
            }else if(val.addr == APB_TB_ADDRESS){
                /* Read data from registers by singel. */
                val.addr = 0x01010101;
                val.offset ++;
                val.data ++;                
            }else{
                chardev_err("Invalid ioc address.\n");
                chardev_info("Get ioctl:val.addr=%x,val.offset=%x,val.data=%x.\n",val.addr,val.offset,val.data);
                return -EFAULT;
            }
        break;
        default:
            chardev_err("Invalid ioc command.\n");
            chardev_info("Get ioctl:val.addr=%x,val.offset=%x,val.data=%x.\n",val.addr,val.offset,val.data);
            return -EFAULT;
        break;
    }
#endif
    return 0;
}

static ssize_t ptp_ctl_read(struct file *file,
                            char *buffer,size_t length, 
                            loff_t *offset)
{
    printk("ptp_ctl_read -->\n");
    return length;
}

static ssize_t ptp_ctl_write(struct file *file,
                             const char *buffer, 
                             size_t length,
                             loff_t *offset)
{
    //struct chardev_ctl_reginfo val;
    //length = copy_from_user(&val,buffer,length);
    /* Write singel data into a definatory address.*/
    printk("ptp_ctl_write -->\n");
    return length;
}

static const struct file_operations ptp_ctl_fops = {
    .owner = THIS_MODULE,
    .open = ptp_ctl_open,
    .release = ptp_ctl_release,
    .unlocked_ioctl = ptp_ctl_ioctl,
    .read = ptp_ctl_read,
    .write = ptp_ctl_write,
};

int ptp_ctl_init(ptp_mtps_ext_t *ext_sig)
{
    int rc;
    local_sig = ext_sig;
    dev.chrdev_name = DRIVER_NAME;
    dev.dev_num = DRIVER_MAJ;
    dev.minor_num = DRIVER_MIN;
    
    // Allocate a major and minor number region for the character device
    dev.dev_num = register_chrdev(dev.dev_num, dev.chrdev_name, &ptp_ctl_fops);
    if (dev.dev_num < 0) {
        chardev_err("Unable to allocate character device region.\n");
        goto ret;
    }
    // Create a device class for our device
    dev.dev_class = class_create(THIS_MODULE, dev.chrdev_name);
    if (IS_ERR(dev.dev_class)) {
        chardev_err("Unable to create a device class.\n");
        rc = PTR_ERR(dev.dev_class);
        goto free_chrdev_region;
    }
    /* Create a device for our module. This will create a file on the
     * filesystem, under "/dev/dev->chrdev_name". */
    dev.device = device_create(dev.dev_class, NULL, MKDEV(dev.dev_num, dev.minor_num), NULL,
                                dev.chrdev_name);
    if (IS_ERR(dev.device)) {
        chardev_err("Unable to create a device.\n");
        rc = PTR_ERR(dev.device);
        goto class_cleanup;
    }
    return 0;

class_cleanup:
    device_destroy(dev.dev_class, MKDEV(dev.dev_num,dev.minor_num));
    class_destroy(dev.dev_class);
free_chrdev_region:
    unregister_chrdev(dev.dev_num, dev.chrdev_name);
ret:
    return rc;
}

void ptp_ctl_exit(void)
{
    // Cleanup all related character device structures
    device_destroy(dev.dev_class, MKDEV(dev.dev_num,dev.minor_num));
    class_destroy(dev.dev_class);
    unregister_chrdev(dev.dev_num, dev.chrdev_name);
    return;
}

