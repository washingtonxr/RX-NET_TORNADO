/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: ptp_frame.c
* @Date: Tur, May 17, 2017 at 9:18:09 AM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.1.17
* @bug lists: No known bugs.
*
*/
#include <linux/kernel.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/mm.h>
#include <linux/slab.h>		/* kmalloc ...*/
#include "ptp_frame.h"

static unsigned char amsdu_counder;
static char mpdu_redy_send[203520];

void init_amsdu(ptp_amsdu_t **elist)
{
	*elist = NULL;
    amsdu_counder = 0;
#ifdef _DEBUG
	printk("AMSDU linklist initializing successful.\n");
#endif
}

void init_ampdu(ptp_signle_mpdu_t **elist)
{
	*elist = NULL;
    
#ifdef _DEBUG
	printk("AMPDU linklist initializing successful.\n");
#endif
}

void clear_amsdu(ptp_amsdu_t *elist)
{
	ptp_amsdu_t *eNext;
#ifdef _DEBUG
    unsigned int counter = 0;
#endif
	while(elist != NULL){
#ifdef _DEBUG		
        counter++;
#endif
		eNext = elist->next;
        //if(elist->body.buff!=NULL)
        kfree(elist->body.buff);
		kfree(elist);
		elist = eNext;
	}
#ifdef _DEBUG
	printk("Clear and free AMSDU list successful(%d).\n",counter);
#endif
}

void clear_ampdu(ptp_signle_mpdu_t *elist)
{
	ptp_signle_mpdu_t *eNext;
#if 0
	if(elist == NULL){
#ifdef _DEBUG
        printk("Linklist is empty.\n");
#endif
		return;
	}
#endif
	while(elist != NULL){
		eNext = elist->next;
		kfree(elist);
		elist = eNext;
	}
    
#ifdef _DEBUG
	printk("Clear and free AMPDU list successful.\n");
#endif
}

/* Append ethernetII data to MSDU link list. */
int append_msdu(ptp_amsdu_t **eNode, char *buff, int len)
{
	ptp_amsdu_t *eTmp;
    ptp_amsdu_t *eHead;
    ptp_amsdu_t *eInsert;
	
    eHead = *eNode;
    eTmp = eHead;
	
 	eInsert = (ptp_amsdu_t *)kmalloc(sizeof(ptp_amsdu_t),GFP_KERNEL);  /* Apply for a Node. */
	if(eInsert == NULL){
		printk("Memory kmalloc failed.\n");
		return -1;
	}
    
	memset(eInsert, 0, sizeof(ptp_amsdu_t));
    
    /* Set MSDU header. */
	eInsert->body.indi = AM_INDI;           /* AMSDU Guard Indicator */
    eInsert->body.seq = amsdu_counder++;    /* Start from 0 to 0xFF. */

	eInsert->body.len = (__u16)len;

    /* Set body. */
    eInsert->body.buff = (char *)kmalloc(len,GFP_KERNEL);  /* kmalloc for buff. */
	if(eInsert->body.buff == NULL){
		printk("Memory kmalloc failed.\n");
		return -1;
	}

    memset(eInsert->body.buff, 0, len);
    memcpy(eInsert->body.buff, buff, len);

    eInsert->next = NULL;
    
    if(eHead == NULL){
        *eNode = eInsert;
    }
    else{
        while(eHead->next != NULL){
            eHead = eHead->next;
        }
         eHead->next = eInsert;
        *eNode = eTmp;
    }
#ifdef _DEBUG
	printk("Append MSDU successful(%d)(%08x)->(%08x).\n",check_amsdu_num(*eNode),(unsigned int)eInsert,(unsigned int)eTmp);
#endif

	return 0;   /* Return append address head. */
}


int check_amsdu_len_threshold(unsigned int aggr_len, unsigned int buf_len, unsigned char mcs_level)
{
	int MPDU_len;
	/* MPDU length = Length(MAC header) + Length(A-MSDU) + Length(FCS). */
	MPDU_len = sizeof(ptp_mHeader_t) + (aggr_len + (sizeof(ptp_amsdu_header_t) + buf_len)) + FCS_LEN;

#ifdef _DEBUG
	printk("MPDU_len=%d,RB_DATA_WIDTH(mcs_level)=%d.\n",MPDU_len,RB_DATA_WIDTH(mcs_level));
#endif

    if(MPDU_len > RB_DATA_WIDTH(mcs_level)){
        return 1;
    }else
        return 0;
}

unsigned int check_amsdu_num(ptp_amsdu_t *msdu_head)
{
    unsigned int du_num = 0;

    while(msdu_head != NULL){
        du_num ++;
        msdu_head = msdu_head->next;
    }
    return du_num;
}

/* Print AMSDU link list. */
void print_amsdu_db(ptp_amsdu_t *msdu_head){

    unsigned int counter = 0;
    unsigned int j;
    
    if(NULL == msdu_head){
        printk("amsdu_db is empty.\n");
        return;
    }else{
        while(NULL != msdu_head){

            printk("MSDU-[%d]:\n",counter++);
            /* Print header. */
            printk("MSDU header----------->:\n");
            
            printk("s_add:");
            for(j=0;j<6;j++)
                printk("%02x ",msdu_head->header.s_add[j]);
            printk("\n");
            
            printk("d_add:");
            for(j=0;j<6;j++)
                printk("%02x ",msdu_head->header.s_add[j]);        
            printk("\n");
            
            printk("resv:%08x\n",msdu_head->header.resv);
            
            /* Frame guard space. */
            printk("MSDU Frame guard space----------->:\n");
            printk("indi:%08x\n",msdu_head->body.indi);
            printk("seq:%08x\n",msdu_head->body.seq);
            printk("len:%08x\n",msdu_head->body.len);
            
            /* Frame body. */
            printk("MSDU Frame body----------->:\n");
            //for(j=0; j<msdu_head->body.len; j++)
            //    printk("%02x ",(char)*(msdu_head->body.buff + j));
            //printk("\n");

            msdu_head = msdu_head->next;
        }

    }

}




int show_mpdu(ptp_signle_mpdu_t *mpdu_head)
{
    //unsigned int i;
    printk("Print ptp_mHeader_t(size:%ld).\n",sizeof(ptp_mHeader_t));
#if 0
    for(i=0; i<sizeof(ptp_mHeader_t); i++){
        printk("%x ", (*(mpdu_head + i)));
    }
    printk("\n");
#endif    
    printk("Print ptp_amsdu_fb_t(size:%ld).\n",sizeof(mpdu_head->header.body_len));
#if 0
    for(i=0; i<mpdu_head->header.body_len; i++){
        printk("%x ", (*(mpdu_head->body + i)));
    }
    printk("\n");
#endif
//    printk("Print FCS:\n");
//    printk("%0x08x\n",mpdu_head->FCS);
    return 0;
}

/* Append MSDU data to MPDU link list. */
int append_mpdu(ptp_amsdu_t *msdu_head, ptp_mHeader_t *mpdu_Head, unsigned int aggr_len, unsigned char mcs_level)
{
	//int *transmit_buf;
	unsigned int buff_len = 0;
	unsigned int prt = 0;
	unsigned int i;
#if 1
    if(aggr_len > 0)
        printk("MPDU lenght = %d, buf num = %d.\n",aggr_len,check_amsdu_num(msdu_head));
#endif
    if(msdu_head == NULL){
        //printk("No content in AMPDU linklist.\n");
        return -1;
    }else{

		//transmit_buf = (int *)mpdu_redy_send;
		buff_len = ((mcs_level+1)*20352);
		memset(mpdu_redy_send, 0, sizeof(mpdu_redy_send));
		
		// Copy MAC header.
		prt += 20;
		// Copy AMSDU body.

        while(msdu_head!= NULL){
            // Copy AMSDU body.
            //transmit_buf[prt/sizeof(int)] = *(int *)(&msdu_head->body.indi);
            //prt += 4;
            mpdu_redy_send[prt++] = msdu_head->body.indi;
			mpdu_redy_send[prt++] = msdu_head->body.seq;
			mpdu_redy_send[prt++] = msdu_head->body.len;
			mpdu_redy_send[prt++] = msdu_head->body.len>>8;
            memcpy(mpdu_redy_send+prt, msdu_head->body.buff , msdu_head->body.len);
            prt += msdu_head->body.len;
            
            msdu_head = msdu_head->next;
        }
    }

#if 0
    printk("MPDU:\n");
    for(i=0;i<buff_len;i++)
        printk("0x%02x ",mpdu_redy_send[i]);
    printk("\nThe end.\n");
#endif

#if 0
    char *mpdu_redy_send;
    int *transmit_buf;
    //int *source_buf;
    int FCS = 0;
    unsigned int buff_len = 0;
    unsigned int prt = 0;
    int i;

    if(aggr_len > 0)
        printk("MPDU lenght = %d, buf num = %d.\n",aggr_len,check_amsdu_num(msdu_head));
    if(msdu_head == NULL){
        //printk("No content in AMPDU linklist.\n");
        return -1;
    }else{
		buff_len = ((mcs_level+1)*20352);
		mpdu_redy_send = (char *)kmalloc(buff_len,GFP_KERNEL); 
		if(mpdu_redy_send == NULL){
			printk("Memory kmalloc failed.\n");
			return -1;
		}
		transmit_buf = (int *)mpdu_redy_send;
		
		memset(mpdu_redy_send, 0, buff_len);
		
		// Copy MAC header.
		prt += 20;
		// Copy AMSDU body.

        while(msdu_head!= NULL){
            // Copy AMSDU body.
            //transmit_buf = (int *)(&msdu_head->body.indi);
            
            //transmit_buf[0] = (int)msdu_head->body.indi;
            transmit_buf[prt/sizeof(int)] = *(int *)(&msdu_head->body.indi);
            prt += 4;
            memcpy(mpdu_redy_send + prt, msdu_head->body.buff , msdu_head->body.len);
            prt += msdu_head->body.len;
            
            msdu_head = msdu_head->next;
        }
    }
    // Caculate FCS and insert into MPDU.
    //transmit_buf[prt/sizeof(int)] = FCS; 
    //prt += 4;
#if 0
    printk("MPDU:\n");
    for(i=0;i<buff_len;i++)
        printk("0x%02x ",mpdu_redy_send[i]);
    printk("\nThe end.\n");
#endif
    kfree(mpdu_redy_send);
#if 0
    eInsert = (ptp_signle_mpdu_t *)kmalloc(sizeof(ptp_signle_mpdu_t),GFP_KERNEL);  /* Apply for a Node. */
    if(eInsert == NULL){
		printk("Memory kmalloc failed.\n");
		return -1;
	}
    
	memset(eInsert, 0, sizeof(ptp_signle_mpdu_t));
    /* Copy MPDU header from mpdu_Head*/
    /* TBD... */
    /* Set MPDU header. */
    eInsert->header.ver=0;
    eInsert->header.m_type=0;
    eInsert->header.s_type=0;
    eInsert->header.extrn=0;
    eInsert->header.aggre=1;
    eInsert->header.retry=0;
    eInsert->header.more_d=0;
    eInsert->header.ack=0;
    eInsert->header.fc_resv=0;
    eInsert->header.body_len=aggr_len;
    /* Set address*/
    
    /* Set body. */
    eInsert->body = msdu_head;
    
    /* Set FCS. */
    eInsert->FCS = 0;  /* Set to 0 temporary. */

    eInsert->next = NULL;
    
    if(eHead == NULL){
        *mpdu_eNode = eInsert;
    }else{
        while(eHead->next != NULL){
            eHead = eHead->next;
        }
        eHead->next = eInsert;
        *mpdu_eNode = eTmp;
    }
#endif
#ifdef _DEBUG
    printk("Append MPDU successful.\n");
#endif
    return 0;
}


/* Print AMPDU link list. */
void print_ampdu_db(ptp_signle_mpdu_t *mpdu_head){

    unsigned int counter = 0;
    unsigned int j;
    
    if(NULL == mpdu_head){
        printk("mpdu_db is empty.\n");
        return;
    }else{
        while(NULL != mpdu_head){

            printk("MPDU-[%d]:\n",counter++);
            
            /* Print header. */
            printk("MPDU header----------->:\n");
            printk("ver:%x \n",mpdu_head->header.ver);
            printk("m_type:%x \n",mpdu_head->header.m_type);
            printk("s_type:%x \n",mpdu_head->header.s_type);
            printk("extrn:%x \n",mpdu_head->header.extrn);
            printk("aggre:%x \n",mpdu_head->header.aggre);
            printk("retry:%x \n",mpdu_head->header.retry);
            printk("more_d:%x \n",mpdu_head->header.more_d);
            printk("ack:%x \n",mpdu_head->header.ack);
            printk("fc_resv:%x \n",mpdu_head->header.fc_resv);

            printk("s_add:");
            for(j=0;j<6;j++)
                printk("%02x ",mpdu_head->header.s_add[j]);
            printk("\n");
            
            printk("d_add:");
            for(j=0;j<6;j++)
                printk("%02x ",mpdu_head->header.d_add[j]);     
            printk("\n");
            
            printk("body_len:%x \n",mpdu_head->header.body_len);

            printk("seq:%x \n",mpdu_head->header.seq);

            printk("qos:%x \n",mpdu_head->header.qos);
            
            /* Frame body. */
            print_amsdu_db(mpdu_head->body);

            printk("FCS:%x \n",mpdu_head->FCS);
            printk("\n");
            mpdu_head = mpdu_head->next;
        }

    }
#endif
}

int duplicate_mpdu_db(ptp_signle_mpdu_t *mpdu_head, ptp_signle_mpdu_t *bk_mpdu_head)
{
    ptp_signle_mpdu_t *t_head;
    
    /* Clear bk_mpdu_head. */
    clear_ampdu(bk_mpdu_head);

    while(mpdu_head != NULL){
        
        /* Kmemory allocate. */
        t_head = (ptp_signle_mpdu_t *)kmalloc(sizeof(ptp_signle_mpdu_t),GFP_KERNEL);  /* Apply for a Node. */
        if(t_head == NULL){
            printk("t_head kmalloc failed.\n");
            return -1;
        }
        
        /* Initialize new memory. */
        memset(t_head,0,sizeof(ptp_signle_mpdu_t));
        
        /* Copy MAC header. */
        memcpy(&t_head->header, &mpdu_head->header, sizeof(ptp_mHeader_t));
        
        /* Copy body. */
        while(t_head->body != NULL){
            t_head->body = (ptp_amsdu_t *)kmalloc(sizeof(ptp_amsdu_t),GFP_KERNEL);  /* Apply for a Node. */
            if(t_head->body == NULL){
                printk("t_head->body kmalloc failed.\n");
                return -1;
            }

            memset(t_head->body,0,sizeof(ptp_amsdu_t));
        
            /* Copy MASDU header. */
            memcpy(&t_head->body->header, &mpdu_head->body->header , sizeof(ptp_amsdu_header_t));

            /* Copy guard space. */
            t_head->body->body.indi = mpdu_head->body->body.indi;
            t_head->body->body.seq = mpdu_head->body->body.seq;
            t_head->body->body.len = mpdu_head->body->body.len;

            /* Copy body data. */
            t_head->body->body.buff = (char *)kmalloc(t_head->body->body.len,GFP_KERNEL);  /* Apply for a Node. */
            if(t_head->body->body.buff == NULL){
                printk("t_head->body->body->buff kmalloc failed.\n");
                return -1;
            }
            
            /* Initialize new memory. */
            memset(t_head->body->body.buff,0,t_head->body->body.len);  /* Critical note: Needa release manually! */
            
            /* Copy Ethernet II data. */
            memcpy(t_head->body->body.buff,mpdu_head->body->body.buff,t_head->body->body.len);
            //t_head->body->body->buff = mpdu_head->body->body->buff;

        }
        /* Copy FCS. */
        t_head->FCS = mpdu_head->FCS;
    }
    return 0;
}







