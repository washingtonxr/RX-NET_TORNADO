/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: ptp_com.c
* @Date: Wed, May 22, 2017 at 16:19:12 PM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.2.0
* @bug lists: No known bugs.
*
*/

#include <linux/timer.h>
#include <linux/wait.h>
#include <linux/kthread.h>
#include <linux/kernel.h>
#include <linux/delay.h>
#include <linux/stddef.h>	/* bool ...*/
#include <linux/slab.h>		/* kmalloc ...*/
#include <linux/atomic.h>

#include "ptp_com.h"

static unsigned long _desired_time_to_elapse = 0;

cominfo_t CMB_setTimeout_ms(uint32_t timeOut_ms)
{
	_desired_time_to_elapse = jiffies + msecs_to_jiffies(timeOut_ms);

	return(COMINFO_OK);
}

cominfo_t CMB_setTimeout_us(uint32_t timeOut_us)
{
	_desired_time_to_elapse = jiffies + usecs_to_jiffies(timeOut_us);

	return(COMINFO_OK);
}

cominfo_t CMB_hasTimeoutExpired(void)
{
	if (time_after(jiffies, _desired_time_to_elapse))
		return (COMINFO_ERR);
	else
		return(COMINFO_OK);
}

static struct task_struct * _mppack_task;	/* Packup MPDU in each 5ms. */
static struct task_struct * _timmer_task;		/* Timer in each 5ms. */
//static bool push_done;
static ptp_mtps_ext_t *ext_sigin;
static unsigned int task_t1, task_t2;
//static unsigned int counter;
static int ptp_check_msbuff(unsigned char type)
{    
#if 0
    if(counter++>=10){
        counter = 0;
    	printk("T:(%d)prt:%d,alen:%d,blen:%d.\n",type,atomic_read(&ext_sigin->msbuff_prt),ext_sigin->msbuff_a_len,ext_sigin->msbuff_b_len);
    	printk("a_we:%d,b_we:%d.\n",atomic_read(&ext_sigin->msbuff_afull),atomic_read(&ext_sigin->msbuff_bfull));
    }
#endif
	//printk("msabuf_add:%08x,msbbuf_add:%08x.\n",ext_sigin->msbuff_a,ext_sigin->msbuff_b);
	//down(&ext_sigin->msgl_lock);
	if(atomic_read(&ext_sigin->msbuff_afull)==1){
        append_mpdu(ext_sigin->msbuff_a,NULL,ext_sigin->msbuff_a_len,ext_sigin->mcs_lev);
        //append_mpdu(ext_sigin->msbuff_a, &ext_sigin->mpbuff_ext, ext_sigin->msbuff_a_len);
		//msleep(5);
		/* Clean AMSDU linklist. */
		//clear_ampdu(ext_sigin->mpbuff_ext);
		//init_ampdu(&ext_sigin->mpbuff_ext);
		
		ext_sigin->msbuff_a_len = 0;
		/* Clean AMSDU linklist. */
		clear_amsdu(ext_sigin->msbuff_a);
		init_amsdu(&ext_sigin->msbuff_a);
		atomic_dec(&ext_sigin->msbuff_afull);
	}else if(atomic_read(&ext_sigin->msbuff_bfull)==1){
        append_mpdu(ext_sigin->msbuff_b,NULL,ext_sigin->msbuff_b_len,ext_sigin->mcs_lev);

		ext_sigin->msbuff_b_len = 0;
		/* Clean AMSDU linklist. */
		clear_amsdu(ext_sigin->msbuff_b);
		init_amsdu(&ext_sigin->msbuff_b);
		atomic_dec(&ext_sigin->msbuff_bfull);
	}else{
		switch(atomic_read(&ext_sigin->msbuff_prt)){
		case 0:	/* Buffer A. */
			//printk("-->1\n");
			atomic_inc(&ext_sigin->msbuff_prt);
        
            append_mpdu(ext_sigin->msbuff_a,NULL,ext_sigin->msbuff_a_len,ext_sigin->mcs_lev);
			
			ext_sigin->msbuff_a_len = 0;
			/* Clean AMSDU linklist. */
			clear_amsdu(ext_sigin->msbuff_a);
			init_amsdu(&ext_sigin->msbuff_a);
			//atomic_dec(&ext_sigin->msbuff_afull);
		break;
		case 1:	/* Buffer B. */
			//printk("-->0\n");
			atomic_dec(&ext_sigin->msbuff_prt);

            append_mpdu(ext_sigin->msbuff_b,NULL,ext_sigin->msbuff_b_len,ext_sigin->mcs_lev);

			init_ampdu(&ext_sigin->mpbuff_ext);
			
			ext_sigin->msbuff_b_len = 0;
			/* Clean AMSDU linklist. */
			clear_amsdu(ext_sigin->msbuff_b);
			init_amsdu(&ext_sigin->msbuff_b);
			//atomic_dec(&ext_sigin->msbuff_bfull);
		break;
		default:
			atomic_set(&ext_sigin->msbuff_prt,0);
			printk("Never supposed to be here.\n");
		break;
		}
	}
	//up(&ext_sigin->msgl_lock);
    return 0;
}
#define TASK2_TO 1
static int ptp_pack_loader(void *data)  
{
	/* start wait */
    //CMB_setTimeout_ms(10);
	do{  
		//printk(KERN_INFO "ptp_pack_load run in: %d times \n", tc);    
		wait_event_interruptible(ext_sigin->_wait_queue,(task_t2 >= TASK2_TO));// || ext_sigin->ms_cpy_sig 

        /* Reset signal .*/
        if(task_t2 >= TASK2_TO ){
		    task_t2 = 0;
        }
        //printk("Wakeup(%d)(Time out) MPDU packup and send.\n",task_t1++);
		ptp_check_msbuff(1);
	}while(!kthread_should_stop());
	return 0;
}

static int ptp_pack_timmer(void *data)  
{  
	do{  
        if(waitqueue_active(&ext_sigin->_wait_queue )){
			wake_up_interruptible(&ext_sigin->_wait_queue);
		}
		//printk(KERN_INFO "ptp_pack_timmer:time out(%d).\n",task_t2++);
		task_t2++;
		msleep_interruptible(MPDU_TO); //MPDU_TO
	}while(!kthread_should_stop());
	return 0;
}

int init_bg_tasks(ptp_mtps_ext_t *ext_sig)
{
    ext_sigin = ext_sig;
    
    task_t1 = task_t2 = 0;
	
	/* Initialize atomic. */
	atomic_set(&ext_sigin->msbuff_prt,0);
	atomic_set(&ext_sigin->msbuff_afull,0);
	atomic_set(&ext_sigin->msbuff_bfull,0);
	
    //spin_lock_init(&ext_sigin->lock);
	sema_init(&ext_sigin->msgl_lock ,1);
	//mutex_init(&ext_sigin->db_lock);
	
	init_waitqueue_head(&ext_sigin->_wait_queue);
    
    /* Start task 1. */
	_mppack_task = kthread_run(ptp_pack_loader, NULL, "MPDU packer");
    
	if (IS_ERR(_mppack_task)){
		printk(KERN_INFO "Packup MPDU thread start failed!\n"); 
	    return -1;		
	}else
		printk(KERN_INFO "Packup MPDU thread start successfully!\n"); 

	/* Start task 2. */
	_timmer_task = kthread_run(ptp_pack_timmer, NULL, "5ms timmer");
    
	if (IS_ERR(_timmer_task)){
		printk(KERN_INFO "5ms timmer thread start failed!\n");  
		return -1;
	}else
		printk(KERN_INFO "5ms timmer thread start successfully!\n");
	
	return 0;
}

int exit_bg_tasks(void)
{

	/* Stopp task 1. */
	if (!IS_ERR(_mppack_task)){  
		int ret = kthread_stop(_mppack_task);  
		printk(KERN_INFO "mpack_task has stopped ,return %d\n", ret);  
	}

    /* Stopp task 2. */
	if (!IS_ERR(_timmer_task)){  
		int ret = kthread_stop(_timmer_task);  
		printk(KERN_INFO "push_task has stopped ,return %d\n", ret);  
	}	
	return 0;
}

