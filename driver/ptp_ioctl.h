/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: ptp_ioctl.h
* @Date: Tur, May 31, 2017 at 18:23:12 PM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.1.1
* @bug lists: No known bugs.
*
*/
#include <linux/types/h>
#include <linux/ioctl.h>
//#include <asm/ioctl.h>              // IOCTL macros
#include "ptp_com.h"

#ifndef PTP_IOCTL_H
#define PTP_IOCTL_H

// The magic number used to distinguish IOCTL's for our device
#define PTP_IOCTL_MAGIC          'f'
// The number of IOCTL's implemented, used for verification
#define PTP_IOCTL_NUM_IOCTLS           2

#define PTP_IOCTL_REG _IOWR(PTP_IOCTL_MAGIC, 11, \
                                unsigned int)

#define PTP_IOCTL_WREG _IOW(PTP_IOCTL_MAGIC, 01, \
                                unsigned int)






#endif

