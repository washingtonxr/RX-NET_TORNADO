#!/bin/sh
echo "clear app"
cd ../app/
make clean
echo "clear module"
cd ../driver/
make clean
echo "uninstall virtnet.ko"
rmmod virtnet.ko
echo "configure br0 and attach interfaces to it."
echo "Try to set up br."
ifconfig br0 down
brctl delif br0 ens33
brctl delif br0 netT
brctl delbr br0
ifconfig ens33 down
ifconfig netT down
/etc/init.d/networking restart
ifconfig ens33 up
ifconfig netT up
ifconfig
echo "Done"

