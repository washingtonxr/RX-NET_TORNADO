echo "change directory to module folder"
cd /lib/modules/4.9.0-xilinx-v2017.4/extra/
echo "enable virtnet module"
insmod virtnet.ko
echo "enable axi-dma module"
insmod l1adapter.ko
echo "enable l1chipset module"
insmod l1chipset.ko
echo "configure br0 and attach interfaces to it."
echo "Try to set up br."
ifconfig -a
brctl addbr br0
ifconfig ens33 down
ifconfig p2pwif0 down
ifconfig eth0 0.0.0.0
ifconfig p2pwif0 0.0.0.0
ifconfig ens33 up
ifconfig p2pwif0 up
brctl addif br0 eth0
brctl addif br0 p2pwif0
ifconfig br0 192.168.206.200 netmask 255.255.255.0 up
ifconfig
echo "Done"

echo "ls /usr/bin"
ls /usr/bin |grep l1adp*

