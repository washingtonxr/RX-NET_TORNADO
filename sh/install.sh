#!/bin/sh
echo "build app"
cd ../app/
make
echo "build module"
cd ../driver/
make
echo "install virtnet.ko"
insmod virtnet.ko
echo "configure br0 and attach interfaces to it."
echo "Try to set up br."
ifconfig -a
brctl addbr br0
ifconfig ens33 down
ifconfig netT down
ifconfig ens33 0.0.0.0
ifconfig netT 0.0.0.0
ifconfig ens33 up
ifconfig netT up
brctl addif br0 ens33
brctl addif br0 netT
ifconfig br0 192.168.188.131 netmask 255.255.255.0 up
ifconfig
echo "Done"

