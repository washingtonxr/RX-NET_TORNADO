/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: main.c
* @Date: Wed, Aug 22, 2018 at 14:23:18 PM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.2.0
* @bug lists: No known bugs.
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>          // Types for open()
#include <sys/stat.h>           // Open() system call
#include <fcntl.h>              // Flags for open()
#include <sys/ioctl.h>
#include <sys/time.h>           // Timing functions and definitions
#include <errno.h>              // Error codes
#include <termios.h> 
#include "main.h"
#include "conversion.h"

unsigned int Fix_time;
/**
 * Prints the usage for this program.
 */
static void print_usage(bool help)
{
    FILE* stream = (help) ? stdout : stderr;
    double default_size;

    fprintf(stream, "Usage:\nnetTornado\t[-n <Transmit time>] [-m <Test mode>] \n"
			"\t\t[-h <Useadge>] [-l <Buffer length>] "
            "\n");
    if (!help) {
        return;
    }
    fprintf(stream,"--------------------------------------------------------------------------------------------------\n");
    fprintf(stream, "\t-n <number transfers>:\t\tThe number of transfers.Default is %d transfers.\n",1);
	fprintf(stream, "\t-m <Test mode.>:\t\tTest mode.\n");
    fprintf(stream, "\t-h <Help info.>:\t\tHelp Information.\n");
    fprintf(stream,"--------------------------------------------------------------------------------------------------\n");                                  
    return;
}
int parse_int(char option, char *arg_str, int *data)
{
    int rc;

    rc = sscanf(optarg, "%d", data);
    if (rc < 0) {
        perror("Unable to parse argument");
        return rc;
    } else if (rc != 1) {
        fprintf(stderr, "Error: Unable to parse argument '-%c %s' as an "
                "integer.\n", option, arg_str);
        return -EINVAL;
    }

    return 0;
}

/**
 * Parses the command line arguments overriding the default transfer sizes,
 * and number of transfer to use for the benchmark if specified. */
static int parse_args(int argc, char **argv, struct chardev_ext_inf *info)
{
    double double_arg;
    int int_arg;
    char option;
	
	info->Try_time = 0;
	info->Buf_lens = 0;
	info->Buf_type = 0;
	info->Mode = 0;
	Fix_time = 0;
    if(argc < 2){
        print_usage(false);
        return -EINVAL;
    }
    while ((option = getopt(argc, argv, "N:n:m:l:h")) != (char)-1)
    {
        switch (option)
        {
            // Retry time.
            case 'N':
                if (parse_int(option, optarg, &int_arg) < 0) {
                    print_usage(false);
                    return -EINVAL;
                }
                Fix_time = int_arg;
                break;
			// Retry time.
            case 'n':
                if (parse_int(option, optarg, &int_arg) < 0) {
                    print_usage(false);
                    return -EINVAL;
                }
                info->Try_time = int_arg;
                break;
            // Set mode.
            case 'm':
                if (parse_int(option, optarg, &int_arg) < 0) {
                    print_usage(false);
                    return -EINVAL;
                }
                info->Mode = int_arg;
                break;
            // Set data length.
            case 'l':
                if (parse_int(option, optarg, &int_arg) < 0) {
                    print_usage(false);
                    return -EINVAL;
                }
                info->Buf_lens = int_arg;
                break;
				// Print detailed usage message
            case 'h':
                print_usage(true);
                exit(0);
                break;
            default:
                print_usage(false);
                return -EINVAL;
        }
    }
	printf("=========================================\n");
	printf("info->Try_time = %d\n",info->Try_time);
	printf("info->Buf_lens = %d\n",info->Buf_lens);
	printf("info->Buf_type = %d\n",info->Buf_type);
	printf("info->Mode = %d\n",info->Mode);
	printf("Fix_time = %d\n",Fix_time);
	printf("=========================================\n");
    return 0;
}

int main(int argc, char **argv)
{
    int i;
    int ret;
    int fd;
    struct chardev_ext_inf info;
    struct timeval tx_time, rx_time;
    struct timeval start_time, end_time;
	double elapsed_time,tx_data_rate;
    
	printf("This is netTornado.\n");
    /* Parse argc & argv, Check if the user overrided the default transfer size and number. */
    if (parse_args(argc, argv, &info) < 0) {
        return -1;
    }

	fd = open("/dev/nTornado", O_RDWR);   // Open nTornado.
	if (fd < 0) {
		printf("Can't open /dev/nTornado\n");
		return -1;
	}

    while(1){
		printf("info.Mode=%d.\n",info.Mode);
		switch(info.Mode){
			case 0:
				// Begin timing
				gettimeofday(&start_time, NULL);
				for(i=0;i<Fix_time;i++){
					if(ioctl(fd, PTP_CTL_IO, &info) < 0) {
						printf("ioctl error.\n");
					}
					//usleep(1);
				}
				// End timing
				gettimeofday(&end_time, NULL);
				elapsed_time = TVAL_TO_SEC(end_time) - TVAL_TO_SEC(start_time);
				// Report the statistics to the user
				printf("DMA Timing Statistics:\n");
				printf("Fix_time = %d.\n",Fix_time);
				printf("\tElapsed Time: %0.8f s\n", elapsed_time);
				if(info.Buf_lens!=0){
					tx_data_rate = BYTE_TO_MB((info.Buf_lens+35)*info.Try_time)*Fix_time / elapsed_time;
					printf("\tTransmit Throughput: %0.2f MiB/s\n", tx_data_rate);
				}
			break;
			case 1:
			break;
			default:
			break;
		}
		
        printf("Ready to exit.\n");
        return 0;
    }

    close(fd);
    return 0;    
}


