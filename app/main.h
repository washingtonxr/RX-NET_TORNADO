/*
* This file contains common code that is intended to be used accross
* board so that it's not replicated.
* 
* Copyright (C) 2018 Washington Ruan
* 
* This software is licensed under the terms of the GNU General Public
* License version 2,as published by the Free Software Foundation, and
* may be copied, distribted, and modified under those terms.
* 
* This program is distributed in the hope that it will be usefull,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
* GNP.
*
* @File: main.h
* @Date: Wed, Aug 08, 2018 at 14:10:12 PM BJT
* @Author: Washington Ruan
* @E-mail: washingtonxr@gmail.com
* @Version: 0.2.0
* @bug lists: No known bugs.
*
*/
#ifndef MAIN_H
#define MAIN_H

struct chardev_ext_inf{
    unsigned char SA[6];            // Source Address.
    unsigned char DA[6];            // Destination Address.
    unsigned int Try_time;          // Try_times.
    unsigned int Buf_lens;          // Buffer data length.
    unsigned char Buf_type;         // Buffer type.
    unsigned char Mode;             // Mode.
};
#define PTP_CTL_MAGIC              'R'
#define AXIDMA_CTL_NUM_IOCTLS		1
#define PTP_CTL_IO	_IOWR(PTP_CTL_MAGIC, 0, struct chardev_ext_inf)

#endif

